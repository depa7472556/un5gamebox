// Fill out your copyright notice in the Description page of Project Settings.


#include "Collectable.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"

// Sets default values
ACollectable::ACollectable()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent=CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent=MeshComponent;
	SphereComponent=CreateDefaultSubobject<USphereComponent>(TEXT("ShereCollision"));
	SphereComponent->InitSphereRadius(300.f);
	SphereComponent->SetupAttachment(MeshComponent);

}

// Called when the game starts or when spawned
void ACollectable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACollectable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


