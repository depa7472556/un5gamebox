// Fill out your copyright notice in the Description page of Project Settings.


#include "NotInteractableActor.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ANotInteractableActor::ANotInteractableActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent=CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent=MeshComponent;

}

// Called when the game starts or when spawned
void ANotInteractableActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANotInteractableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

