// Fill out your copyright notice in the Description page of Project Settings.


#include "Clients.h"
#include "Components/SphereComponent.h"

// Sets default values
AClients::AClients()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SphereComponent=CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	RootComponent=SphereComponent;

}

// Called when the game starts or when spawned
void AClients::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AClients::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

