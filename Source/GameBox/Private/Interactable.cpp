// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
//#include "Components/WidgetComponent.h"

// Sets default values
AInteractable::AInteractable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent=CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent=MeshComponent;
	SphereComponent=CreateDefaultSubobject<USphereComponent>(TEXT("ShereCollision"));
	SphereComponent->InitSphereRadius(300.f);
	SphereComponent->SetupAttachment(MeshComponent);

	
}

// Called when the game starts or when spawned
void AInteractable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInteractable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

