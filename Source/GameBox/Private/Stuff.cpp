// Fill out your copyright notice in the Description page of Project Settings.


#include "Stuff.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AStuff::AStuff()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent=CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent=MeshComponent;

}

// Called when the game starts or when spawned
void AStuff::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStuff::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


