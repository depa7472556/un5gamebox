// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameBox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GameBox, "GameBox" );
