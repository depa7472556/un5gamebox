// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Stuff.generated.h"


class UStaticMeshComponent;

UCLASS()
class GAMEBOX_API AStuff : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AStuff();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent*MeshComponent;

	
};
